from django.db import models

class GoogleAddressField(models.ForeignKey):
    description = 'An address'

    def __init__(self, related_name=None, **kwargs):
        kwargs['to'] = 'google_address.GoogleAddress'
        kwargs['related_name'] = related_name
        kwargs['null'] = True
        kwargs['blank'] = True
        super(GoogleAddressField, self).__init__(**kwargs)
