from rest_framework import serializers

from google_address.models import GoogleAddress
from lib.serializers.model_serializer import DefaultModelSerializer


def google_address_in_validated_data(field, validated_data):
    """
    Helper method to save address fields
    To be used in the Update and Create method of parents of AddressSerializer
    Transforms address field dict to a Address instance or None
    :param field: name of address field (string)
    :param validated_data: serializer's validated_data
    :return: Modified `validated_data`, address dict -> GoogleAddress instance OR None
    """
    if field in validated_data:
        data = validated_data.pop(field, None)

        raw = data.get('raw', None)
        if not raw:
            validated_data[field] = None
        else:
            address = GoogleAddressSerializer(data=data)
            address.is_valid(raise_exception=True)
            address.save()

            validated_data[field] = address.instance


def find_google_address_in_validated_data(validated_data):
    """
    Helper method to save address fields
    This is a wrapper method for 'address_in_validated_data' to find fields prefixed with 'address'
    :param validated_data: serializer's validated_data
    :return: Modified `validated_data`, address dict -> Address instance
    """
    for key, val in validated_data.items():
        if key[:7] == 'address':
            google_address_in_validated_data(key, validated_data)


class GoogleAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = GoogleAddress
        exclude = ()

    def create(self, validated_data):
        ModelClass = self.Meta.model

        # this is the only mandatory field so it is used to fetch the record
        raw = validated_data.get('raw')

        instance = ModelClass.objects.get_or_create(raw=raw)[0]

        # performing an update style here because updates are ignored in favour of this method
        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()

        return instance

    def update(self, instance, validated_data):
        return self.create(validated_data)


class AddressModelSerializer(DefaultModelSerializer):
    """
    Model Serializer with added ability to save address field.
    Address field name must be prefixed with 'address'
    """

    def update(self, instance, validated_data):
        find_google_address_in_validated_data(validated_data)

        return super(AddressModelSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        find_google_address_in_validated_data(validated_data)

        return super(AddressModelSerializer, self).create(validated_data)


class AddressInSerializerMixin(object):
    def update(self, instance, validated_data):
        find_google_address_in_validated_data(validated_data)

        return super(AddressInSerializerMixin, self).update(instance, validated_data)

    def create(self, validated_data):
        find_google_address_in_validated_data(validated_data)

        return super(AddressInSerializerMixin, self).create(validated_data)
