from __future__ import unicode_literals

from django.db import models
from rest_framework.exceptions import ValidationError


class GoogleAddress(models.Model):
    raw = models.CharField(max_length=200, null=True, blank=True)
    formatted = models.CharField(max_length=200, null=True, blank=True)
    street_number = models.CharField(max_length=20, null=True, blank=True)
    route = models.CharField(max_length=100, null=True, blank=True)
    unit = models.CharField(max_length=20, null=True, blank=True)

    locality = models.CharField(max_length=165, null=True, blank=True)
    postal_code = models.CharField(max_length=10, null=True, blank=True)

    region_name = models.CharField(max_length=165, null=True, blank=True)
    region_code = models.CharField(max_length=12, null=True, blank=True)

    country = models.CharField(max_length=80, null=True, blank=True)

    place_id = models.CharField(max_length=200, null=True, blank=True)

    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)

    def clean(self):
        if not self.raw:
            raise ValidationError('Addresses may not have a blank `raw` field.')
