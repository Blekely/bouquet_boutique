from __future__ import unicode_literals

from django.apps import AppConfig


class GoogleAddressConfig(AppConfig):
    name = 'google_address'
