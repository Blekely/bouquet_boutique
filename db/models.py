from __future__ import unicode_literals

from django.db import models

from google_address.fields import GoogleAddressField


class Flower(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(max_length=255)


class Hardgood(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(max_length=255)


class Wholesaler(models.Model):
    name = models.ImageField(max_length=255)
    address = GoogleAddressField(related_name='wholesaler_address')
    web_site = models.URLField()
    email = models.EmailField()
    phone_num = models.ImageField(max_length=255)
    account_number = models.ImageField(max_length=255)


class WholesalerFlowerPrice(models.Model):
    flower = models.ForeignKey(Flower, related_name='flower')
    wholesaler = models.ForeignKey(Wholesaler, related_name='wholesaler')
    price_bunch = models.DecimalField(max_digits=10, decimal_places=2)
    quantity_bunch = models.SmallIntegerField()
